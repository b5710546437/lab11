import java.io.IOException;
import java.util.Scanner;

import com.lloseng.ocsf.client.AbstractClient;



public class Client extends AbstractClient
{
	final static String server = "158.108.137.11";
	final static int port = 5555;

	public Client(String host, int port) {
		super(host, port);
	}

	@Override
	protected void handleMessageFromServer(Object msg) 
	{
		System.out.println("> "+(String)msg);
		

	}

	public static void main(String [] args)
	{
		Scanner scan = new Scanner(System.in);
		Client c = new Client(server,port);

		try {
			c.openConnection();
			System.out.println("Connected to server "+server);
			while(c.isConnected()){
				
				String attempt = scan.nextLine().trim();
				if(attempt.equals("quit"))
					break;
				c.sendToServer(attempt);
			}
			c.closeConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
