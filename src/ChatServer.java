import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;


public class ChatServer extends AbstractServer 
{

	final static int LOGGEDIN = 2;
	final static int WHISPER = 1;
	final static int LOGGEDOUT = 0;
	private List<ConnectionToClient> clients = new ArrayList<ConnectionToClient>();

	public ChatServer(int port) {
		super(port);
	}

	@Override
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		if(!(msg instanceof String)){
			sentToClient(client,"unknown message");
			return;
		}
		String message = (String)msg;
		int state = (Integer)client.getInfo("state");

		switch(state){
		case LOGGEDOUT:
			if(message.matches("Login \\w+")){
				String username = message.substring(6).trim();
				client.setInfo("username",username);
				client.setInfo("state", LOGGEDIN);
				super.sendToAllClients(client.getInfo("username")+" connected.");
			}
			else{
				sentToClient(client, "Please login");
			}
			break;
		case LOGGEDIN:
			if(message.equalsIgnoreCase("Logout"))
			{
				client.setInfo("state", LOGGEDOUT);
				super.sendToAllClients(client.getInfo("username")+" disconnected.");
			}
			else if(message.matches("To: \\w+")){
				String username = message.substring(4).trim();	
				for(ConnectionToClient c : clients){
					if(username.equals((String)c.getInfo("username"))){
						client.setInfo("whisper",c.getInfo("username"));
						client.setInfo("state", WHISPER);
					}
				}
			}
			else
			{
				super.sendToAllClients(client.getInfo("username") + " : " + message);
			}
			break;
		case WHISPER:
			for(ConnectionToClient c : clients){
				if(client.getInfo("whisper").equals((String)c.getInfo("username"))){
					if((int)c.getInfo("state")==LOGGEDOUT)
						sentToClient(client,c.getInfo("username")+" is not logged in.");
					else
						sentToClient(c,message);
				}
			}

			client.setInfo("state", LOGGEDIN);
			break;




		}
	}

	private void sentToClient(ConnectionToClient client, String msg) {
		try {
			client.sendToClient(client.getInfo("username") + " --> "+(String)msg);
		} catch (IOException e) {

		}

	}

	protected void clientConnected(ConnectionToClient client)
	{
		client.setInfo("state", LOGGEDOUT);
		clients.add(client);

	}

	protected synchronized void clientDisconnected(ConnectionToClient client){
		super.sendToAllClients("Disconnected :  "+client.getInfo("username"));
		clients.add(client);
	}

	public static void main(String [] args){
		Scanner scan = new Scanner(System.in);
		ChatServer server = new ChatServer(5555);
		try{
			server.listen();
			while(server.isListening()){

			}
			server.close();

		}catch(IOException e){
			e.printStackTrace();
		}


	}

}
